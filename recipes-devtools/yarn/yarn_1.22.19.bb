# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

SUMMARY = "📦🐈 Fast, reliable, and secure dependency management."
# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   LICENSE
LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=9afb19b302b259045f25e9bb91dd34d6"

SRC_URI = " \
    npm://registry.npmjs.org/;package=yarn;version=${PV} \
    "

S = "${WORKDIR}/npm"

inherit npm

LICENSE:${PN} = "BSD-2-Clause"

BBCLASSEXTEND = "native nativesdk"
