SUMMARY = "index.html"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI += " \
    file://index2.html \
    "

do_install() {
    install -d ${D}${WWWDIR}/html
    install -m 0644 ${WORKDIR}/index2.html ${D}${WWWDIR}/html
}
